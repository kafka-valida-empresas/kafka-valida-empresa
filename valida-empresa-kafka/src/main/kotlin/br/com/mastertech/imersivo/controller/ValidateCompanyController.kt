package br.com.mastertech.imersivo.controller

import br.com.mastertech.imersivo.kafka.models.Company
import br.com.mastertech.imersivo.service.PublishCompany
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable


@FeignClient(name = "checkcompanycontroller", url = "https://www.receitaws.com.br")
interface ValidateCompanyController {

    @GetMapping(value = ["/v1/cnpj/{cnpj}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun get(@PathVariable("cnpj") cnpj: String) : String

}