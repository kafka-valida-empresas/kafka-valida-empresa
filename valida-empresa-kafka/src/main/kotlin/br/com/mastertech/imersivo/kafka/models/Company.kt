package br.com.mastertech.imersivo.kafka.models

data class Company(val cnpj: String
                   , var capital: String = ""
                   , var name:String = ""
                   , var isAcceptable: Boolean = false)