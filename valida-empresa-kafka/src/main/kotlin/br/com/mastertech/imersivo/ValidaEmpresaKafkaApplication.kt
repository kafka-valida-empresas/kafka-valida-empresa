package br.com.mastertech.imersivo

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
@EnableAutoConfiguration(exclude = [org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration::class])
class ValidaEmpresaKafkaApplication

fun main(args: Array<String>) {
	runApplication<ValidaEmpresaKafkaApplication>(*args)
}
