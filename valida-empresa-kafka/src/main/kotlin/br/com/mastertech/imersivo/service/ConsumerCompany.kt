package br.com.mastertech.imersivo.service

import br.com.mastertech.imersivo.controller.ValidateCompanyController
import br.com.mastertech.imersivo.kafka.models.Company
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration
import org.springframework.http.converter.json.GsonBuilderUtils
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component


@Component
class ConsumerCompany {

    @Autowired
    lateinit var controller: ValidateCompanyController

    @Autowired
    lateinit var publisher: PublishCompany


    @KafkaListener(topics = ["chrislucas-biro-1"], groupId = "chrisluccas1")
    fun listener(@Payload company: Company) {
        println(company)
        val jsonResult = controller
                .get(company.cnpj.replace(Regex("\\D+"), ""))
        println(jsonResult)

        val jsonElement = JsonParser.parseString(jsonResult);

        if (jsonElement.isJsonObject) {
            val jsonObject = jsonElement.asJsonObject
            val capital = jsonObject["capital_social"].asDouble
            val nome = jsonObject["nome"].asString
            publisher.publish(Company(company.cnpj, capital.toString(), nome,capital >= 1000000.0f))
        }
    }
}